import React from 'react'

import { ExampleComponent } from 'uau-toast'
import 'uau-toast/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
