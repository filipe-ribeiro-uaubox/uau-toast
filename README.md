# uau-toast

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/uau-toast.svg)](https://www.npmjs.com/package/uau-toast) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save uau-toast
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'uau-toast'
import 'uau-toast/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [uaubox-ribeirofilipe](https://github.com/uaubox-ribeirofilipe)
